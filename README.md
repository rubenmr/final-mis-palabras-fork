# Entrega practica
Practica final MisPalabras 

## Datos
* Nombre: Rubén Manzanas Rodríguez
* Titulación: Ingeniería en Telemática
* Despliegue (url): http://rmanzanas2017.pythonanywhere.com
* Video básico (url): https://youtu.be/ZWaJlfivBxo
* Video parte opcional (url): https://youtu.be/JQHRiINA0So
* Usuario Laboratorio: rubenmr
* Usuario GitLab: rubenmr

## Cuenta Admin Site
* admin/admin

## Cuentas usuarios
* ruben/ruben
* pepe/pepe


## Resumen parte obligatoria
La parte obligatoria se ha realizado como se expone en el enunciado, incluyendo todos los apartados obligatorios. Un ejemplo de funcionamiento aparece en la url del video básico.

## Lista partes opcionales
* Inclusión de favicon.
* Busqueda de palabras con acentos, palabras con un espacio entre medias y nombre personales.
* Posibilidad de borrar una palabra.
* Posibilidad de borrar un usuario.

## Incidentes
Al desplegar la práctica en pythonanywhere, no permite ver en su totalidad el entorno gráfico, ya que en la cabecera de la página tendría que aparecer el fondo de una imagen y este no aparece, en su lugar aparece un fondo blanco y no permite ver el título de la página, debido letras del titulo son blancas. Tampoco permite buscar ni almacenar la definición Rae de una palabra.

