from django.urls import path

from . import views

urlpatterns = [
        path('', views.inicio),
        path('iniciar_sesion', views.iniciar_sesion),
        path('registrarse', views.registrarse),
        path('borrar', views.borrar_sesion),
        path('logout', views.logout_view),
        path('usuario', views.pagina_usuario),
        path('ayuda', views.ayuda),
        path('palabra/', views.tratar_palabra, name='palabra'),
]

