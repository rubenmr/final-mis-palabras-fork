from django import forms
from django.contrib.auth.models import User


#formulario para el ususario
class UsuarioForm(forms.ModelForm):
    class Meta:
        model = User
        fields =('username', 'password')