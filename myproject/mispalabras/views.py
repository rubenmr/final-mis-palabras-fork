from django.shortcuts import render, redirect
import json
import urllib
from xml.dom import minidom
from bs4 import BeautifulSoup
from django.contrib.auth import logout, authenticate, login
from django.core.paginator import Paginator
from django.http import HttpResponse, JsonResponse
import xml.etree.cElementTree as ET
from .forms import UsuarioForm
from .models import Usuario, Palabras, Comentario, Votos, Informacion, Tarjeta
import xmltodict
from django.contrib.auth.models import User
from django import utils
# Create your views here.

def inicio(request):
    try:
        page = request.GET['p']
    except:
        page = "1" 
    try:
        format = request.GET['format']
    except:
        format = ""
    if format == "xml":
       return servirXML()
    if format == "json":
        return servirJSON()
    lista_palabras = Palabras.objects.all().order_by('-fecha')
    p = Paginator(lista_palabras,5)
    palabras = p.get_page(page)

    context = {
        'user_authenticated': request.user.is_authenticated,
        'user': request.user,
        'in_inicio': True,
        'palabras': palabras,
        'masvotados': masvotados(),
        'num_palabras': len(Palabras.objects.all())
    }
    return render(request, 'mispalabras/inicio.html', context)

def registrarse(request):
    if request.method == "POST":
        form = UsuarioForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            usuario = Usuario(usuario=user)
            usuario.save()
        return render(request, "mispalabras/iniciar_sesion.html")
    else:
        return render(request, 'mispalabras/registrarse.html')

def iniciar_sesion(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            body = "Bienvenido"
            authenticate_user = True
            context = {
                'titulo': "Iniciar sesion",
                'user_authenticated': request.user.is_authenticated,
                'user': username,
                'content': body,
                'masvotados': masvotados(),
                'num_palabras': len(Palabras.objects.all()),
            }
            return render(request, "mispalabras/pagina_sesion.html", context)
        else:
            return render(request, 'mispalabras/registrarse.html')
    else:
        return render(request, 'mispalabras/iniciar_sesion.html')

def borrar_sesion(request):
    if request.method == "GET":
        User.objects.get(username=request.user.username, password=request.user.password).delete()  
        context = {
                'titulo': "Usuario borrado",
                'masvotados': masvotados(),
                'num_palabras': len(Palabras.objects.all()),
            }          
        return render(request, "mispalabras/usuario_borrado.html", context)
    else:
        return render(request, 'mispalabras/pagina_sesion.html')

def logout_view(request):
    logout(request)
    return  redirect("/")

def pagina_simple(request,title, body):
    context = {
        'titulo': title,
        'user_authenticated': request.user.is_authenticated,
        'user': request.user,
        'content': body,
        'masvotados': masvotados(),
        'num_palabras': len(Palabras.objects.all())
    }
    return render(request, "mispalabras/pagina_simple.html", context)


def error(request,mensaje_error):
    return pagina_simple(request,"Error",mensaje_error)

def palabra_almacenada_page(request, palabra_buscada):
    return pagina_simple(request, palabra_buscada, palabra_buscada)

def palabra_no_añadida(request, palabra):
    context = {
        'user_authenticated': request.user.is_authenticated,
        'user': request.user,
        'palabra': palabra,
        'masvotados': masvotados(),
        'num_palabras': len(Palabras.objects.all())
    }
    return render(request, 'mispalabras/buscar_palabra.html', context)

def numVotos(palabra):
    votos = 0
    for voto in Votos.objects.filter(palabra=palabra):
        votos = votos + 1
    return votos

def usuario_ya_votado(usuario,palabra):
    if Votos.objects.filter(palabra=palabra,user=usuario):
        return True
    else:
        return False

def pagina_palabra(request, palabra):
    context = {
        'user_authenticated': request.user.is_authenticated,
        'user': request.user,
        'palabra': palabra,
        'comentarios': Comentario.objects.filter(palabra=palabra),
        'num_votos': numVotos(palabra),
        'usuario_no_voto': not(usuario_ya_votado(request.user,palabra)),
        'masvotados': masvotados(),
        'num_palabras': len(Palabras.objects.all()),
        'rae': Informacion.objects.filter(palabra=palabra,lugar="RAE"),
        'Flickr': Informacion.objects.filter(palabra=palabra,lugar="Flickr"),
        'Meme': Informacion.objects.filter(palabra=palabra,lugar="Meme"),
        'urls': Tarjeta.objects.filter(palabra=palabra)
    }
    return render(request, 'mispalabras/añadir_palabra.html', context)

def wiki_pedia(palabra_buscada):
    palabra = {}
    palabra['valor'] = palabra_buscada
    try:
        url_descripcion = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + urllib.parse.quote(palabra_buscada).replace(' ','+') + "&prop=extracts&exintro&explaintext"
        xmlStream = urllib.request.urlopen(url_descripcion)
        doc = minidom.parse(xmlStream)
        extract = doc.getElementsByTagName("extract")[0]
        body = extract.firstChild.data
        page = doc.getElementsByTagName("page")[0]
        page_id = page.getAttribute('pageid')
    except:
        body = "Esta palabra no está en Wikipedia. "
        page_id = 0

    palabra['descripcion'] = body

    try:
        url_foto = "https://es.wikipedia.org/w/api.php?action=query&titles="+ urllib.parse.quote(palabra_buscada).replace(' ','+') + "&prop=pageimages&format=json&pithumbsize=200"
        jsonStream = urllib.request.urlopen(url_foto)
        jsonObject = json.load(jsonStream)
        foto = jsonObject['query']['pages'][page_id]['thumbnail']['source']
    except:
        foto = ""
    palabra['id'] = page_id
    palabra['foto'] = foto
    return palabra

def guaradar_palabra(request,palabra_buscada):
    palabra_datos = wiki_pedia(palabra_buscada)
    palabra = Palabras(palabra=palabra_datos['valor'], descripcion=palabra_datos['descripcion'],
        foto=palabra_datos['foto'], user=request.user, page= palabra_datos['id'])
    palabra.save()
    return palabra

def guardar_comentario(request,palabra,contenido):
    c = Comentario(palabra=palabra,user=request.user,contenido=contenido)
    c.save()

def guardar_voto(request,palabra):
    if not(usuario_ya_votado(request.user,palabra)):
        c = Votos(palabra=palabra,user=request.user)
        palabra.num_votos = palabra.num_votos + 1
        palabra.save()
        c.save()
        return True
    else:
        return False

def guardar_rae(request,palabra):
    if Informacion.objects.filter(palabra=palabra,lugar="RAE"):
        return False
    url = "https://dle.rae.es/"+ urllib.parse.quote(palabra.palabra).replace(' ', '+')
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        soup = BeautifulSoup(response, 'html.parser', from_encoding=response.info().get_param('charset'))
        if soup.findAll("meta", property="og:description"):
            definition = soup.find("meta", property="og:description")['content']
        else:
            definition=  "No tiene definición en la RAE"
    info = Informacion(palabra=palabra,user=request.user,lugar="RAE",info=definition)
    info.save()
    return True

def guardar_meme(request,palabra):
    if Informacion.objects.filter(palabra=palabra,lugar="Meme"):
        return False
    url_meme = "http://apimeme.com/meme?meme=Baby-Godfather&top="+urllib.parse.quote(palabra.palabra).replace(' ', '+') + "+&bottom=Yessss"
    info = Informacion(palabra=palabra, user=request.user, lugar="Meme", info=url_meme)
    info.save()
    return True

def borrar_palabra(palabra):
    if Informacion.objects.filter(palabra=palabra,lugar="Borrar Palabra"):
        return False
    palabr = Palabras.objects.filter(palabra=palabra)
    palabr.delete()
    return True

def guardar_link(request,palabra,url):
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        soup = BeautifulSoup(response, 'html.parser', from_encoding=response.info().get_param('charset'))
        if soup.findAll("meta", property="og:description"):
            definition = soup.find("meta", property="og:description")['content']
        else:
            definition = "No tiene definición"
        if soup.findAll("meta", property="og:image"):
            imagen = soup.find("meta", property="og:image")['content']
        else:
            imagen = "None"
    info = Tarjeta(palabra=palabra, user=request.user, descripcion=definition, foto=imagen,url=url)
    info.save()
    return True

def guardar_fliker(request,palabra):
    url_xml = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + urllib.parse.quote(palabra.palabra).replace(' ', '+')
    xml = urllib.request.urlopen(url_xml)
    xml_dict = xmltodict.parse(xml.read())
    imagen_flickr = xml_dict["feed"]["entry"][0]["link"][1]["@href"]
    info = Informacion(palabra=palabra, user=request.user, lugar="Flickr", info=imagen_flickr)
    info.save()
    return True

def tratar_palabra(request):
    if request.method == 'GET':
        try:
            palabra_buscada = request.GET["word"]
            if palabra_buscada == " ":
                return redirect("/")
            if Palabras.objects.filter(palabra=palabra_buscada):
                palabra_datos = Palabras.objects.get(palabra=palabra_buscada)
                return pagina_palabra(request, palabra_datos)
            else:
                palabra = wiki_pedia(palabra_buscada)
                if (palabra['id'] != 0) and (Palabras.objects.filter(page=palabra['id'])):
                    palabra = Palabras.objects.get(page=palabra['id'])
                    return pagina_palabra(request,palabra)
                return palabra_no_añadida(request, palabra)
        except utils.datastructures.MultiValueDictKeyError:
            return redirect("/")
    elif request.method == 'POST':
        form = request.POST["form"]
        if form == "Añadir Palabra":
            if request.user.is_authenticated:
                try:
                    palabra_buscada = request.POST["nueva_palabra"]
                    if palabra_buscada == " ":
                        return redirect("/")
                    if Palabras.objects.filter(palabra=palabra_buscada):
                        return error(request, "Palabra añadida")
                    else:
                        palabra_datos = guaradar_palabra(request, palabra_buscada)
                        return pagina_palabra(request, palabra_datos)

                except utils.datastructures.MultiValueDictKeyError:
                    return redirect("/")
            else:
                return error(request,"Inicia sesion para subir una palabra")
        if form == "Añadir Comentario":
            if request.user.is_authenticated:
                try:
                    palabra = request.POST["coment_palabra"]
                    contenido = request.POST["coment"]
                    if palabra == " ":
                        return redirect("/")
                    if Palabras.objects.filter(palabra=palabra):
                        palabra_datos = Palabras.objects.get(palabra=palabra)
                        guardar_comentario(request,palabra_datos,contenido)
                        return pagina_palabra(request, palabra_datos)
                    else:
                        palabra = wiki_pedia(palabra)
                        if (palabra['id'] != 0) and (Palabras.objects.filter(page=palabra['id'])):
                            palabra = Palabras.objects.get(page=palabra['id'])
                            guardar_comentario(request, palabra, contenido)
                            return pagina_palabra(request, palabra)
                        return palabra_no_añadida(request, palabra)

                except utils.datastructures.MultiValueDictKeyError:
                    return redirect("/")
            else:
                return error(request, "Inicia sesion para subir un comentario")
        if form == "Votar":
            if request.user.is_authenticated:
                try:
                    palabra = request.POST["vote_palabra"]
                    if palabra == " ":
                        return redirect("/")
                    if Palabras.objects.filter(palabra=palabra):
                        palabra_datos = Palabras.objects.get(palabra=palabra)
                        if guardar_voto(request,palabra_datos):
                            return pagina_palabra(request, palabra_datos)
                        else:
                            return error(request,"Ya has votado")
                    else:
                        palabra = wiki_pedia(palabra)
                        if (palabra['id'] != 0) and (Palabras.objects.filter(page=palabra['id'])):
                            palabra = Palabras.objects.get(page=palabra['id'])
                            if guardar_voto(request, palabra):
                                return pagina_palabra(request, palabra)
                            else:
                                return error(request, "Ya has votado")
                        return palabra_no_añadida(request, palabra)

                except utils.datastructures.MultiValueDictKeyError:
                    return redirect("/")
            else:
                return error(request, "Inicia sesion para votar una palabra")         
        if form == "Añadir Info":
            if request.user.is_authenticated:
                try:
                    palabra = request.POST["palabra_info"]
                    if palabra == " ":
                        return redirect("/")
                    if Palabras.objects.filter(palabra=palabra):
                        palabra_datos = Palabras.objects.get(palabra=palabra)
                        info_type = request.POST["info_type"]
                        if info_type == "RAE" and guardar_rae(request,palabra_datos):
                            return pagina_palabra(request, palabra_datos)
                        elif info_type == "Flickr" and guardar_fliker(request,palabra_datos):
                            return pagina_palabra(request, palabra_datos)
                        elif info_type == "Meme" and guardar_meme(request,palabra_datos):
                            return pagina_palabra(request, palabra_datos)
                        elif info_type == "Borrar Palabra" and borrar_palabra(palabra_datos):
                            return redirect("/")
                        else:
                            return error(request,"Ya se ha almacenado esa informacion")
                    else:
                        palabra = wiki_pedia(palabra)
                        if (palabra['id'] != 0) and (Palabras.objects.filter(page=palabra['id'])):
                            palabra = Palabras.objects.get(page=palabra['id'])
                            if guardar_rae(request, palabra):
                                return pagina_palabra(request, palabra)
                            else:
                                return error(request, "Ya se ha almacenado")
                        return palabra_no_añadida(request, palabra)

                except utils.datastructures.MultiValueDictKeyError:
                    return redirect("/")
            else:
                return error(request, "Inicia sesion para subir la informacion de la palabra")
        if form == "Añadir Url":
            if request.user.is_authenticated:
                try:
                    palabra = request.POST["palabra"]
                    url = request.POST["url"]
                    if palabra == " ":
                        return redirect("/")
                    if Palabras.objects.filter(palabra=palabra):
                        palabra_datos = Palabras.objects.get(palabra=palabra)
                        guardar_link(request,palabra_datos,url)
                        return pagina_palabra(request, palabra_datos)
                    else:
                        palabra = wiki_pedia(palabra)
                        if (palabra['id'] != 0) and (Palabras.objects.filter(page=palabra['id'])):
                            palabra = Palabras.objects.get(page=palabra['id'])
                            guardar_link(request, palabra, url)
                            return pagina_palabra(request, palabra)
                        return palabra_no_añadida(request, palabra)

                except utils.datastructures.MultiValueDictKeyError:
                    return redirect("/")
            else:
                return error(request, "Inicia sesion para subir un comentario")
    else:
        return redirect("/")

def pagina_usuario(request):
    if request.user.is_authenticated:
        usuario = request.user
        context = {
            'usuario': usuario,
            'user_authenticated': request.user.is_authenticated,
            'user': request.user,
            'comentarios': Comentario.objects.filter(user=usuario.id).order_by("-fecha"),
            'palabras': Palabras.objects.filter(user=usuario.id).order_by("-fecha"),
            'in_mipagina': True,
            'masvotados': masvotados(),
            'num_palabras': len(Palabras.objects.all()),
            'informacion': Informacion.objects.filter(user=request.user).order_by("-fecha"),
            'urls': Tarjeta.objects.filter(user=request.user).order_by("-fecha")
        }
        return render(request, 'mispalabras/pagina_usuario.html', context)
    else:
        return error(request,"Usuario no encontrado")

def ayuda(request):
    context = {
        'user_authenticated': request.user.is_authenticated,
        'user': request.user,
        'masvotados': masvotados(),
        'num_palabras': len(Palabras.objects.all())
    }
    return render(request, "mispalabras/ayuda.html", context)

def masvotados():
    return Palabras.objects.all().order_by("-num_votos")[:10]

def generarJSON():
    data = {}
    data['palabras'] = []
    for palabra in Palabras.objects.all().order_by("-fecha"):
        data['palabras'].append({
            'palabra': palabra.palabra,
            'href': "http://127.0.0.1:8000/palabra/?word=" + palabra.palabra})
    return data

def servirJSON():
    return JsonResponse(generarJSON())

def generarXML():
    prtg = ET.Element("palabras")
    for palabra in Palabras.objects.all().order_by("-fecha"):
        result = ET.SubElement(prtg, 'palabra')
        ET.SubElement(result, "palabra").text = palabra.palabra
        ET.SubElement(result, "href").text = "http://127.0.0.1:8000/palabra/?word=" + palabra.palabra

    rough_string = ET.tostring(prtg, 'utf-8').decode('utf8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def servirXML():
    return HttpResponse(generarXML(),content_type="text/xml")



