from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Usuario(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.usuario

class Palabras (models.Model):
    palabra = models.TextField()
    page = models.IntegerField(default=0)
    descripcion = models.TextField()
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    num_votos = models.BigIntegerField(default=0)
    foto = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.palabra


class Comentario(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE) 
    contenido = models.TextField()
    palabra = models.ForeignKey(Palabras,on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.palabra

class Votos(models.Model):
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    user = models.TextField()


class Informacion(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    palabra = models.ForeignKey(Palabras,on_delete=models.CASCADE)
    info = models.TextField()
    lugar = models.TextField()  
    fecha = models.DateTimeField(auto_now_add=True)

class Tarjeta(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    palabra = models.ForeignKey(Palabras,on_delete=models.CASCADE)
    url = models.TextField()
    descripcion = models.TextField()
    foto = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)
    
    
    
