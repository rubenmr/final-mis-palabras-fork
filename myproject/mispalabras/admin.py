from django.contrib import admin
from django.db import models
from .models import Usuario, Palabras, Comentario, Votos, Informacion, Tarjeta

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Palabras)
admin.site.register(Comentario)
admin.site.register(Votos)
admin.site.register(Informacion)
admin.site.register(Tarjeta)